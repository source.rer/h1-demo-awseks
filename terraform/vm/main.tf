#
# Terraform
#
terraform {
    required_version = ">= v0.12.6"
    backend "s3" {
        encrypt = true
        bucket = "tfstate-847991"
        key = "vm-mgmt.tfstate"
        dynamodb_table = "tfstate-lock"
    }
}

variable "WHAT_IS_MY_IP" {
  default = "0.0.0.0"
}

#
# Key pair
#
resource "aws_key_pair" "default_key_pair" {
  key_name = "default_key_pair"
  public_key = "${file("files/id_rsa.pub")}"
}

#
# Provider
#
provider "aws" {}

#
# Security Group
#
resource "aws_security_group" "mgmtsecuritygroup" {
  name          = "mgmtsecuritygroup"
  description   = "Mgmt VM firewall"
  
  # Relax rules (test pourposes)
  # ingress {
  #  from_port   = 0
  #  to_port     = 0
  #  protocol    = "-1"
  #  cidr_blocks = ["0.0.0.0/0"]
  # }

  # Inbound
  ingress {
   from_port   = 0
   to_port     = 0
   protocol    = "-1"
   cidr_blocks = ["${var.WHAT_IS_MY_IP}/32"]
   description = "Office HQ"
  }

  # Outbound
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow all"
  }
}

resource "aws_instance" "mgmtinstance" {
  ami                     = "ami-05c1fa8df71875112" # Ubuntu 18.04
  instance_type           = "t2.micro"
  key_name                = "default_key_pair"

  security_groups         = [ "mgmtsecuritygroup" ]

  user_data               = "${file("${path.module}/files/userdata.sh")}"

  tags = {
    Name = "mgmt"
  }
}

output "vm-public-ip" {
  value = ["${aws_instance.mgmtinstance.*.public_ip}"]
}
