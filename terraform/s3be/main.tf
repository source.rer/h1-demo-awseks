provider "aws" {}

resource "aws_s3_bucket" "tfstate" {
    bucket = "tfstate-847991"

    versioning {
      enabled = true
    }
    
    # Prevent destroy by default
    #force_destroy = true
    lifecycle {
      prevent_destroy = true  
    }
}
